angular.module('gifme.services', [])
.factory('Device', function($ws) {
	var Device =  {
		all : function() {
			return $ws.perform('GET', 'device');
		}
	};
	return Device;
})
.factory('Images', function($ws) {
	var Images = {
		all : function() {
			return $ws.perform('GET', 'image');
		},
		deviceImages : function(deviceId) {
			return $ws.perform('GET', 'image/device/'+deviceId);
		},
		remove : function(imageId) {
			return $ws.perform('DELETE', 'image/' + imageId); 
		}
	};
	return Images;
})
.factory('$ws', function($q, $http, $httpParamSerializer) {
	// if(params) params = $httpParamSerializer(params);
	var $ws = {
		basePath : '../',
		perform : function(method, service, params) {
			var dfd = $q.defer();
			$http({
				method: method,
				url: this.basePath + service,
				data: params,
				headers: {
					'Accept': 'application/json'
				},
			})
			.success(function successCallback(response) {
				dfd.resolve(response);
			})
			.error(function errorCallback(response) {
				dfd.reject(response);
			});
			return dfd.promise;
		}
	};

	return $ws;
})

.factory('Utils', function() {
	return {
		arrayChunk : function(array, size) {
			//declare vars
			var output = [];
			var i = 0; //the loop counter
			var n = 0; //the index of array chunks
			
			for(var item in array) {
				
				//if i is > size, iterate n and reset i to 0
				if(i >= size) {
					i = 0;
					n++;
				}
				
				//set a value for the array key if it's not already set
				if(!output[n] || output[n] == 'undefined') {
					output[n] = [];
				}
				
				output[n][i] = array[item];
				
				i++;
				
			}
			
			return output;
		}
	};
});