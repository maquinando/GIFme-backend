angular.module('gifme.controllers', [])
.controller('AppCtrl', function($scope, $log, $mdSidenav, $mdDialog, $mdLightbox, $mdBottomSheet, Device, Images, Utils) {

	$scope.showDelete = false;
	$scope.selected = null;
	$scope.devices = [];
	Device.all()
	.then(function(data) {
		if(data.code == 200) {
			$scope.devices = data.data;
		}
	}, function(err){
		$log.error(err);
	});


	$scope.count = 0;
	$scope.images = [];
	$scope.toggleDevice = function() {
		$scope.images = [];
		if($scope.selected === null) {
			Images.all()
			.then(function(data){
				$scope.count = data.data.length;
				$scope.images = Utils.arrayChunk(data.data, 4);
			});
		} else {
			Images.deviceImages($scope.selected)
			.then(function(data){
				$scope.count = data.data.length;
				$scope.images = Utils.arrayChunk(data.data, 4);
			});
		}
	};
	$scope.toggleList = function() {
		var pending = $mdBottomSheet.hide() || $q.when(true);

		pending.then(function(){
			$mdSidenav('left').toggle();
		});
	};
	$scope.toggleDelete = function() {
		$scope.showDelete = !$scope.showDelete;
	}
	$scope.selectDevice = function ( deviceId ) {
		if(!deviceId) deviceId = null;
		$scope.selected = deviceId;
		$scope.toggleDevice();
		$scope.toggleList();
	};

	$scope.removeItem = function(image) {
		Images.remove(image.id)
		.then(function(data) {
			// image removed
			angular.forEach($scope.images, function(row, key) {
				angular.forEach(row, function(img,index) {
					if(img.id === image.id){
						$scope.images[key].splice(index,1);
					}
				});
			});
		}, function() {
			$mdDialog.show(
				$mdDialog.alert()
				.parent(angular.element(document.querySelector('#popupContainer')))
				.clickOutsideToClose(true)
				.title('Error')
				.textContent('Error al tratar de eliminar la imágen.')
				.ariaLabel('Alert Dialog Demo')
				.ok('Got it!')
				.targetEvent(ev)
				);
		});
	};

	$scope.getImage = function(image) {
		if(image) {
			return '../classes/uploads/' + image.path;
		}
		return null;
	};
	$scope.showImage = function(image) {
		$mdLightbox.showLightboxModal($scope.getImage(image), image.path);
	};

	$scope.uploadImage = function() {
		var confirm = $mdDialog.confirm({
			templateUrl: "uploadTest.html",
			clickOutsideToClose: true,
			controller: uploadTestCtrl
		});

		$mdDialog.show(confirm);
	};
	function uploadTestCtrl($scope, $mdDialog) {
		$scope.data = {};
		$scope.title = title;

		$scope.cancel = function() {
			$mdDialog.cancel();
		};

	}
	$scope.toggleDevice();
});