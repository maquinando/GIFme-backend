(function(){
	'use strict';

	angular.module('gifme', [ 'ngMaterial', 'ngRoute' ])
	.config(['$routeProvider',function($routeProvider) {
		$routeProvider.
		when('/all', {
			templateUrl: 'views/gif-image-list.html',
			controller: 'ImagesCtrl'
		}).
		when('/phones/:phoneId', {
			templateUrl: 'partials/phone-detail.html',
			controller: 'PhoneDetailCtrl'
		}).
		otherwise({
			redirectTo: '/phones'
		});
	}]);


})();
