angular.module('mdLightbox', ['ngMaterial'])
.directive('mdLightbox', ['$mdLightbox', function($mdLightbox){
	return {
		link: function($scope, elem, attrs){

			elem.addClass('image-click');

			elem.on('click',function(){
				var image = attrs.src;
				var title = attrs.mdLightboxTitle;
				$mdLightbox.showLightboxModal(image, title);
			});
		}
	};
}])
.service('$mdLightbox', ['$mdDialog', function($mdDialog) {
	var template = '<md-dialog aria-label="Lightbox"><md-toolbar><div class="md-toolbar-tools"><h2 ng-if="title" ng-bind="title"></h2><span flex></span><md-button class="md-icon-button" ng-click="cancel()"><md-icon md-font-library="material-icons" aria-label="Close" class="icon-static">close</md-icon></md-button></div></md-toolbar><md-dialog-content><img ng-src="{{image}}" alt="{{title}}"></md-dialog-content></md-dialog>';
	this.showLightboxModal = function(image, title){
		var confirm = $mdDialog.confirm({
			templateUrl: 'lightbox.html',
			clickOutsideToClose: true,
			controller: lightboxController
		});

		$mdDialog.show(confirm);

		function lightboxController($scope, $mdDialog) {
			$scope.image = image;
			$scope.title = title;

			$scope.cancel = function() {
				$mdDialog.cancel();
			};

		}
	};
}]);