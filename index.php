<?php

// Include epiphany framework.
include_once 'vendor/Epiphany/Epi.php';

// Include config parameters.
$settings = array(
	'db' => array(
		'host' => '97.74.149.173',
		'name' => 'dbArrowPhoto',
		'user' => 'dbArrowPhoto',
		'pass' => 'Arrow123!',
		)
	);

// Include route handlers
include_once 'classes/Images.php';
include_once 'classes/Device.php';

include_once 'controllers/DashboardController.class.php';


// Initialize Epiphany framework parameters
Epi::setPath('base', 'vendor/Epiphany');
Epi::setPath('view', './views');
Epi::setSetting('exceptions', true);
Epi::init('api','database', 'template');

EpiDatabase::employ('mysql', $settings['db']['name'], $settings['db']['host'], $settings['db']['user'], $settings['db']['pass']);


/*
 * We create 3 normal routes (think of these are user viewable pages).
 * We also create 2 api routes (this of these as data methods).
 *  The beauty of the api routes are they can be accessed natively from PHP
 *    or remotely via HTTP.
 *  When accessed over HTTP the response is json.
 *  When accessed natively it's a php array/string/boolean/etc.
 */
// getRoute()->get('/', 'showEndpoints');
// getRoute()->get('/version', 'showVersion');
// getRoute()->get('/users', 'showUsers');
// getRoute()->get('/users/javascript', 'showUsersJavaScript');
// getRoute()->get('/params', 'showParams');
// getApi()->get('/version.json', 'apiVersion', EpiApi::external);
// getApi()->get('/users.json', 'apiUsers', EpiApi::external);
// getApi()->get('/params.json', 'apiParams', EpiApi::external);
// getApi()->get('/params-internal.json', 'apiParams', EpiApi::internal);
// getApi()->get('/params-internal-from-external.json', 'apiParamsFromExternal', EpiApi::external);
getRoute()->get('/', 'showEndpoints',EpiApi::external);
getApi()->post('/upload', array('Images', 'uploadGIFImage'), EpiApi::external);

getApi()->post('/device', array('Device', 'register'), EpiApi::external);
getApi()->get('/device/(\w+)', array('Device', 'getDeviceData'), EpiApi::external);
getApi()->get('/device', array('Device', 'all'), EpiApi::external);


getApi()->get('/image', array('Images', 'all'), EpiApi::external);
getApi()->get('/image/device/(\w+)', array('Images', 'deviceImages'), EpiApi::external);

getApi()->delete('/image/(\w+)', array('Images', 'removeImage'), EpiApi::external);

// View routes declarations
// getRoute()->get('/dashboard', array('DashboardController', 'display'));

getRoute()->get('.*', 'error404');
getRoute()->post('.*', 'error404');


getRoute()->run();





/**
 * Show routes enpoints
 */
function showEndpoints() {
  return "";
}

function error404() {
  header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found");
}