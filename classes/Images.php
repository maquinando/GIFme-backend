<?php

define ('SITE_ROOT', realpath(dirname(__FILE__)));

/*
 * ******************************************************************************************
 * Define functions and classes which are executed by EpiRoute
 * ******************************************************************************************
 */
class Images
{
	/**
	 * [MyMethod description]
	 */
	static public function uploadGIFImage()
	{
		// if(!isset($_POST['deviceId'])) {
		// 	header($_SERVER['SERVER_PROTOCOL'] . ' 500 No deviceId sent', true, 500);
		// 	$result =  array(
		// 		'code' => 500,
		// 		'message' => 'No deviceId sent!',
		// 		'debug' => $_POST
		// 		);
		// 	return $result;
		// }
		
		if(!isset($_FILES['image'])) {
			header($_SERVER['SERVER_PROTOCOL'] . ' 500 No image file attached', true, 500);
			$result =  array(
				'code' => 500,
				'message' => 'No image file attached!',
				'debug' => $_FILES
				);
			return $result;
		} 

		$device = getDatabase()->one('SELECT * FROM device WHERE id = :deviceId', array('deviceId' => $_POST['deviceId']));
		if(empty($device)) {
			// header($_SERVER['SERVER_PROTOCOL'] . ' 500 Device not found', true, 500);
			// $result =  array(
			// 	'code' => 204,
			// 	'message' => 'Device with id ' . $deviceId . ', not found.',
			// 	'debug' => $_POST
			// 	);
			// return $result;
			$deviceId = null;
		}
		$uploaddir = 'uploads';
		$uploaddir = SITE_ROOT . DIRECTORY_SEPARATOR . $uploaddir;

		if ( !file_exists($uploaddir) ){
			$result = array(
				'code' => 500,
				'message' => 'Upload directory doesn\'t exists',
				'debug' => realpath($uploaddir)
				);
			return $result;
		} 
		if( !is_dir($uploaddir) ) {
		    // header($_SERVER['SERVER_PROTOCOL'] . ' 500 Unexpected error', true, 500);
			$result = array(
				'code' => 500,
				'message' => 'Upload directory is not directory',
				'debug' => realpath($uploaddir)
				);
			return $result;
		}
		if( !is_writable($uploaddir) ) {
		    // header($_SERVER['SERVER_PROTOCOL'] . ' 500 Unexpected error', true, 500);
			$result = array(
				'code' => 500,
				'message' => 'Upload directory is not writeable',
				'debug' => realpath($uploaddir)
				);
			return $result;
		}

		$filter = isset($_POST['filter'])? $_POST['filter'] : false;
		if(!empty($filter)) {
			switch ($filter) {
				case 'blue':
					# code...
					break;
				
				case 'red':
					# code...
					break;
				
				default:
					# code...
					break;
			}
		}

		$acceptTerms = isset($_POST['accept_terms'])? $_POST['accept_terms'] : false;
		$publishEvent = isset($_POST['publish_event'])? $_POST['publish_event'] : false;
		$publishNetworks = isset($_POST['publish_networks'])? $_POST['publish_networks'] : false;
		$extension = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);
		$filename = self::gen_uuid();
		$uploadfile = $uploaddir . DIRECTORY_SEPARATOR . $filename . '.' . $extension;

		$result = array(
			'code' => 200,
			'message' => 'file upload success'
			);
		try {
			if (move_uploaded_file($_FILES['image']['tmp_name'], $uploadfile)) {

				$deviceId = $_POST['deviceId'];

				//Register in db the uploaded file name by deviceId
				$pictureId = getDatabase()->execute('INSERT INTO `picture` (path, device_id, accept_terms, filter, publish_networks, publish_event) VALUES (:uploadfile, :deviceId, :acceptTerms, :filter, :publishNetworks, :publishEvent)', array('uploadfile' => $filename . '.' . $extension, 'deviceId' => $deviceId, 'acceptTerms' => $acceptTerms, 'filter' => $filter, 'publishNetworks' => $publishNetworks, 'publishEvent' => $publishEvent));

				$result = array(
					'code' => 200,
					'message' => 'file upload success',
					'data' => array(
						'pictureId' => $pictureId
						)
					);
			} else {
				header($_SERVER['SERVER_PROTOCOL'] . ' 500 Unexpected error', true, 500);
				$result = array(
					'code' => 500,
					'message' => 'Possible file upload attack!',
					'debug' => $_FILES
					);
			}
		} catch (Exception $e) {
			header($_SERVER['SERVER_PROTOCOL'] . ' 500 Unexpected error', true, 500);
			$result = array(
				'code' => 500,
				'message' => $e->getMessage()
				);
		} catch (EpiDatabaseQueryException $e) {
  			header($_SERVER['SERVER_PROTOCOL'] . ' 500 Unexpected error', true, 500);
  			$result =  array(
				'code' => 500,
				'message' => $e->getMessage()
				);
  		}
		return $result;
	}

	static public function all() {
		$data = getDatabase()->all('SELECT * FROM `picture` p WHERE published = 1 ORDER BY created DESC');
		foreach ($data as $key => $value) {
			$deviceId = $value['device_id'];
			$data[$key]['device'] = getDatabase()->one('SELECT * FROM `device` WHERE id = :id', array('id' => $deviceId));
			unset($data[$key]['device_id']);
		}
		$result =  array(
			'code' => 200,
			'message' => 'Success!',
			'data' => $data
			);
		return $result;
	}
	static public function deviceImages($deviceId) {
		$data = getDatabase()->all('SELECT * FROM `picture` p WHERE device_id = :deviceId AND published = 1  ORDER BY created DESC', array('deviceId' => $deviceId));
		foreach ($data as $key => $value) {
			$deviceId = $value['device_id'];
			$data[$key]['device'] = getDatabase()->one('SELECT * FROM `device` WHERE id = :id', array('id' => $deviceId));
			unset($data[$key]['device_id']);
		}
		$result =  array(
			'code' => 200,
			'message' => 'Success!',
			'data' => $data
			);
		return $result;
	}
	static public function removeImage($imageId) {
		$count = getDatabase()->execute('update picture set published = 0 where id = :imageId', array('imageId' => $imageId));
		$result = array(
			'code' => 200,
			'message' => $count.' images removed'
			);
		return $result;
	}
	/**
	 * [uploadImage description]
	 * @return Object method results
	 */ 
	static public function uploadImage()
	{
		if(isset($_GET['name']))
			getCache()->set('name', $_GET['name']);

		$name = getCache()->get('name');
		if(empty($name))
			$name = '[Enter your name]';
		echo '<h1>Hello '. $name . '</h1><p><form><input type="text" size="30" name="name"><br><input type="submit" value="Enter your name"></form></p>';
	}

	static private function gen_uuid($len=8) {

		$hex = md5("yourSaltHere" . uniqid("", true));

		$pack = pack('H*', $hex);
		$tmp =  base64_encode($pack);

		$uid = preg_replace("#(*UTF8)[^A-Za-z0-9]#", "", $tmp);

		$len = max(4, min(128, $len));

		while (strlen($uid) < $len)
			$uid .= gen_uuid(22);

		return substr($uid, 0, $len);
	}

}


?>