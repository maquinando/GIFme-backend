<?php
/**
 * Device management
 */

class Device
{
	static public function all()
	{
		$data = getDatabase()->all('SELECT * FROM `device`');
		$result =  array(
			'code' => 200,
			'message' => 'Success!',
			'data' => $data
			);
		return $result;
	}
	static public function getDeviceData($deviceId)
	{
		$data = getDatabase()->one('SELECT * FROM `device` WHERE id = :id', array('id' => $deviceId));
		if(empty($data)) {
			$result =  array(
				'code' => 204,
				'message' => 'No data found!',
				'debug' => $data
				);
		} else {
			$result =  array(
				'code' => 200,
				'message' => 'Success!',
				'data' => $data
				);
		}
		return $result;
	}
	static public function register()
	{
		if(!isset($_POST['uid'])) {
			header($_SERVER['SERVER_PROTOCOL'] . ' 500 No device uid', true, 500);
			$result =  array(
				'code' => 500,
				'message' => 'No device uid!',
				'debug' => $_POST
				);
			return $result;
		}
		// if(!isset($_POST['name'])) {
		// 	header($_SERVER['SERVER_PROTOCOL'] . ' 500 No device name', true, 500);
		// 	$result =  array(
		// 		'code' => 500,
		// 		'message' => 'No device name!',
		// 		'debug' => $_POST
		// 		);
		// 	return $result;
		// }
		$name = isset($_POST['name'])? $_POST['name'] : "";
		$deciceInfo = array(
			'uid' => $_POST['uid'],
			'name' => $name,
			'systemName' => isset($_POST['systemName'])? $_POST['systemName']:null,
			'systemVersion' => isset($_POST['systemVersion'])? $_POST['systemVersion']:null,
			'systemModel' => isset($_POST['model'])? $_POST['model']:null,
			);
		$aux = getDatabase()->one('SELECT * FROM `device` WHERE uid=:uid', array('uid' => $_POST['uid']));
		if( !empty($aux) ) {
			getDatabase()->execute('UPDATE `device` SET uid = :uid, name = :name, systemName = :systemName, systemVersion = :systemVersion, systemModel = :systemModel WHERE uid = :uid ;', $deciceInfo);

			$result =  array(
				'code' => 200,
				'message' => 'Device updated!',
				'data' => array(
					'deviceId' => $aux['id']
					)
				);
			return $result;
		}
		try {
  			$deviceId = getDatabase()->execute('INSERT INTO `device` (uid, name, systemName, systemVersion, systemModel) VALUES (:uid, :name, :systemName, :systemVersion, :systemModel)', $deciceInfo);
  			$result =  array(
				'code' => 200,
				'message' => 'Device registered!',
				'data' => array(
					'deviceId' => $deviceId
					)
				);
  		} catch (EpiDatabaseQueryException $e) {
  			header($_SERVER['SERVER_PROTOCOL'] . ' 500 Unexpected error', true, 500);
  			$result =  array(
				'code' => 500,
				'message' => $e->getMessage(),
				'debug' => $e
				);
  		}
  		
			return $result;
	}
}